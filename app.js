/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc The main app.js file
 */

var express = require('express')
var cors = require('cors')
var fs = require('fs')
var path = require('path')
const logger = require('morgan')
const bodyParser = require('body-parser')
const swaggerUI = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const routes = require('./src/utils/routes')

var postsRouter = require('./src/routes/posts_router')

const app = express()

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

app.use(logger('combined', { stream: accessLogStream }))

app.use(cors())
app.use(express.json())

app.use(routes.BASE_ROUTE + routes.POSTS_ROUTE, postsRouter)
app.use(routes.BASE_ROUTE + routes.DOCS_ROUTE, swaggerUI.serve, swaggerUI.setup(swaggerDocument))
app.use(routes.BASE_ROUTE + routes.HEALTH_CHECK_ROUTE, (req, res) => {
  res.status(200).send()
})

module.exports = app
