/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create date 2020-05-08
 * @modify date 2020-05-08
 * @desc JWT verifier middleware
 */

const { verifyJWTToken } = require('../utils/auth');
var http = require("http");
const constants = require('../utils/constants');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * 
 * verifies a token
 */

function verifyJWT(req, res, next) {

  let token = req.headers.token;
  verifyJWTToken(token)
  .then((decodedToken) => {
    req.user = decodedToken.data
    next()
  })
  .catch((err) => {
    res.status(400)
      .json({
        message: constants.invalidToken
    })
})
}


module.exports = {
verifyJWT
}


