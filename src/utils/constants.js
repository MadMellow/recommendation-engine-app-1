/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */


const getRelatedQuery = (postQueryLimit) => `SELECT ID, post_title, post_date, guid, (SELECT group_concat(wt.name separator ', ') FROM wp_terms wt INNER JOIN wp_term_taxonomy wtt
    on wt.term_id = wtt.term_id INNER JOIN wp_term_relationships wtr on
    wtr.term_taxonomy_id = wtt.term_taxonomy_id WHERE taxonomy = 'category' and
    wp_posts.ID = wtr.object_id ) AS "Categories",
    (SELECT group_concat(wt.name separator ', ') FROM wp_terms wt INNER JOIN wp_term_taxonomy wtt on
    wt.term_id = wtt.term_id INNER JOIN wp_term_relationships wtr on
    wtr.term_taxonomy_id = wtt.term_taxonomy_id WHERE taxonomy = 'post_tag' and wp_posts.ID = wtr.object_id )
    AS "Tags" FROM wp_posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date desc limit ${postQueryLimit}
    `

const getPostQuery = (postId) => {
    return `SELECT ID, post_title, post_date, guid, (SELECT group_concat(wt.name separator ', ') FROM wp_terms wt INNER JOIN wp_term_taxonomy wtt
    on wt.term_id = wtt.term_id INNER JOIN wp_term_relationships wtr on
    wtr.term_taxonomy_id = wtt.term_taxonomy_id WHERE taxonomy = 'category' and
    wp_posts.ID = wtr.object_id ) AS "Categories",
    (SELECT group_concat(wt.name separator ', ') FROM wp_terms wt INNER JOIN wp_term_taxonomy wtt on
    wt.term_id = wtt.term_id INNER JOIN wp_term_relationships wtr on
    wtr.term_taxonomy_id = wtt.term_taxonomy_id WHERE taxonomy = 'post_tag' and wp_posts.ID = wtr.object_id )
    AS "Tags" FROM wp_posts WHERE ID = ${postId} ORDER BY post_date desc limit 1
    `
}

const verifyMessage = "verified";

const invalidToken = "Invalid auth token provided.";

module.exports = {
    getPostQuery,
    getRelatedQuery,
    verifyMessage,
    invalidToken
}