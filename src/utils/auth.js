/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create date 2020-05-08 
 * @modify date 2020-05-08 
 * @desc Auth Utility
 */

const jwt = require('jsonwebtoken');
const constants = require('./constants')


/**
 * 
 * @param {*} token 
 * verifyJWTToken verifies a token using our secret stored in env
 */

function verifyJWTToken(token) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
        if (err || !decodedToken) {
            return reject(err);  
          }
          console.log(constants.verifyMessage);
          resolve(decodedToken);
        });    
      });    
    }    
    
    /**
     *  createJWTToken creates a token using HS256 and an expiry time limit.
     */
        
    function createJWTToken() {
        const token = jwt.sign(    
        {    
          data: {},    
        },    
        process.env.JWT_SECRET,    
        {    
          expiresIn: 1440,    
          algorithm: 'HS512',    
        }    
      );
    
      return token;    
    }    
    
    module.exports = {    
      verifyJWTToken,    
      createJWTToken    
    };
    
