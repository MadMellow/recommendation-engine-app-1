/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */


const app = require('../../app.js');
const supertest = require('supertest');
const request = supertest(app);
const routes = require('../utils/routes');
const { createJWTToken } = require('../utils/auth');

const URLRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

it('returns an error when given an invalid post id', async (done) => {
  const token = createJWTToken();
  const response = await request
    .get(routes.BASE_ROUTE + '/post/related/123')
    .set('Authorization', `Bearer ${token}`);
  expect(response.status).toBe(400);
  done();
});

it('returns 3 urls when given valid post id', async (done) => {
  const token = createJWTToken();
  const response = await request
    .get(routes.BASE_ROUTE + '/post/related/1545')
    .set('Authorization', `Bearer ${token}`);
  expect(response.status).toBe(200);
  expect(Array.isArray(response.body.relatedPosts)).toBe(true);
  expect(response.body.relatedPosts.length).toBe(3);
  expect(response.body.relatedPosts[0].url.match(URLRegex)).toBeTruthy();
  expect(response.body.relatedPosts[1].url.match(URLRegex)).toBeTruthy();
  expect(response.body.relatedPosts[2].url.match(URLRegex)).toBeTruthy();
  done();
});