/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */


const express = require('express')
const postsController = require('../controllers/posts_controller')
const routes = require('../utils/routes')
const { verifyJWT } = require('../middleware');

const postsRouter = express.Router()

postsRouter.all('*', verifyJWT)

postsRouter.get(routes.RELATED_POSTS_ROUTE, postsController.getRelatedPosts)

module.exports = postsRouter
