/**
 * @author Saurabh
 * @email saurabh.mishra@embibe.com
 * @create 
 * @modify 
 * @desc [description]
 */

const constants = require('../utils/constants')
const db = require('../db/posts')

// Matches the posts to the selected post by category and returns top 3 guids
const filterPostsByCategory = (selectedPost, posts) => {
  const postCategories = selectedPost.Categories.split(',')  
  
  let relatedPosts = []

  posts.forEach((v) => {
    if (v.Categories && v.Categories.length > 0) {
      const categories = v.Categories.split(',')
      const intersection = postCategories.filter(element => categories.includes(element))
      if (intersection && intersection.length > 0) {
        relatedPosts.push({ ...v, intersection, intersectionLength: intersection.length })
      }  
    }
  })

  relatedPosts.sort((x, y) => {
    if (x.intersectionLength < y.intersectionLength) {
      return 1
    }
    if (x.intersectionLength > y.intersectionLength) {
      return -1
    }
    return 0
  })
  
  return {
    relatedPosts: relatedPosts.slice(0, 3).map((v) =>  {
      return {
        name: v.post_title,
        url: v.guid
      }
    })
  }
}

const getRelatedPosts = async (postId) => {
  try {
    const selectedPost = await db.getPost(postId)
    const posts = await db.getPosts()
    return filterPostsByCategory(selectedPost, posts)
  } catch(error) {
    console.error(error)
  }
}

module.exports = {
  getRelatedPosts
}
